package database

import "log"

//Create database and tables for work
func Create(name string) error {
	var err error

	_, err = db.Exec("CREATE DATABASE IF NOT EXISTS " + name)
	if err != nil {
		log.Println("Cannot create database")
		return err
	}

	_, err = db.Exec("USE " + name)
	if err != nil {
		log.Println("Cannot use database")
		return err
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS country " +
		"(code CHAR (2) NOT NULL, " +
		"name VARCHAR (100) NOT NULL, " +
		"area INT NOT NULL, " +
		"population INT NOT NULL, " +
		"capital VARCHAR (100) NOT NULL," +
		"PRIMARY KEY (code))")
	if err != nil {
		log.Println("Cannot create <country> table")
		return err
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS user " +
		"(id INT NOT NULL AUTO_INCREMENT, " +
		"name VARCHAR (100) NOT NULL, " +
		"age INT NOT NULL, " +
		"male CHAR (1) NOT NULL, " +
		"PRIMARY KEY (id))")
	if err != nil {
		log.Println("Cannot create <user> table")
		return err
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS task " +
		"(id INT NOT NULL AUTO_INCREMENT, " +
		"user_id INT NOT NULL, " +
		"task VARCHAR (100) NOT NULL, " +
		"PRIMARY KEY (id), " +
		"FOREIGN KEY (user_id) REFERENCES user(id) " +
		"ON DELETE CASCADE " +
		"ON UPDATE CASCADE)")
	if err != nil {
		log.Println("Cannot create <task> table")
		return err
	}

	_, err = db.Exec("CREATE TABLE IF NOT EXISTS contacts " +
		"(user_id INT NOT NULL, " +
		"mail VARCHAR (100) NOT NULL, " +
		"phone VARCHAR (100) NOT NULL, " +
		"PRIMARY KEY (user_id), " +
		"FOREIGN KEY (user_id) REFERENCES user(id) " +
		"ON DELETE CASCADE " +
		"ON UPDATE CASCADE)")
	if err != nil {
		log.Println("Cannot create <contacts> table")
		return err
	}

	return err
}
