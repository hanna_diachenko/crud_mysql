package database

import "log"

//InitDB is initialization of database
func InitDB() error {
	initError := Open()
	if initError != nil{
		log.Println("Error connection to  database")
		return initError
	}

	initError = Create("myBase")
	if initError != nil{
		log.Println("Creation error")
		return initError
	}
	return initError
}
