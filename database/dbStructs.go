package database

//Country describes Country entity
type Country struct {
	Code       string `json:"code"`
	Name       string `json:"name"`
	Area       int    `json:"area"`
	Population int    `json:"population"`
	Capital    string `json:"capital"`
}

//User describes User entity
type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
	Male string `json:"male"`
}

//Task describes User Task
type Task struct {
	ID     int    `json:"id"`
	UserID int    `json:"user_id"`
	Task   string `json:"task"`
}

//Contacts describes Contacts entity
type Contacts struct {
	UserID int    `json:"user_id"`
	Mail   string `json:"mail"`
	Phone  string `json:"phone"`
}
