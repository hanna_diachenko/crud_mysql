package database

import (
	"encoding/json"
	"log"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

//GetCountries returns all countries from database
func GetCountries() ([]interface{}, error) {
	rows, err := db.Query("SELECT * FROM country")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	result := make([]interface{}, 0)
	for rows.Next() {
		var country Country
		sErr := rows.Scan(&country.Code, &country.Name, &country.Area, &country.Population, &country.Capital)

		if sErr != nil {
			return nil, sErr
		}

		result = append(result, country)

		if sErr = rows.Err(); sErr != nil {
			return nil, sErr
		}
	}

	return result, nil
}

//GetUsers returns all users from database
func GetUsers() ([]interface{}, error) {
	rows, err := db.Query("SELECT * FROM user")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	result := make([]interface{}, 0)
	for rows.Next() {
		var user User
		sErr := rows.Scan(&user.ID, &user.Name, &user.Age, &user.Male)

		if sErr != nil {
			return nil, sErr
		}

		result = append(result, user)

		if sErr = rows.Err(); sErr != nil {
			return nil, sErr
		}
	}

	return result, nil
}

//GetTasks returns all tasks from database
func GetTasks() ([]interface{}, error) {
	rows, err := db.Query("SELECT * FROM task")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	result := make([]interface{}, 0)
	for rows.Next() {
		var task Task
		sErr := rows.Scan(&task.ID, &task.UserID, &task.Task)

		if sErr != nil {
			return nil, sErr
		}

		result = append(result, task)

		if sErr = rows.Err(); sErr != nil {
			return nil, sErr
		}
	}

	return result, nil
}

//GetAllContacts returns all contacts from database
func GetAllContacts() ([]interface{}, error) {
	rows, err := db.Query("SELECT * FROM contacts")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	result := make([]interface{}, 0)
	for rows.Next() {
		var contacts Contacts
		sErr := rows.Scan(&contacts.UserID, &contacts.Mail, &contacts.Phone)

		if sErr != nil {
			return nil, sErr
		}

		result = append(result, contacts)

		if sErr = rows.Err(); sErr != nil {
			return nil, sErr
		}
	}

	return result, nil
}

//GetCountry returns country by code
func GetCountry(key string) (Country, error) {
	var country Country

	row := db.QueryRow("SELECT * FROM country WHERE code = ?", key)

	sErr := row.Scan(&country.Code, &country.Name, &country.Area, &country.Population, &country.Capital)

	return country, sErr
}

//GetUser returns user by id
func GetUser(key string) (User, error) {
	var user User

	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return user, err
	}

	row := db.QueryRow("SELECT * FROM user WHERE id = ?", id)

	sErr := row.Scan(&user.ID, &user.Name, &user.Age, &user.Male)

	return user, sErr
}

//GetTask returns task by id
func GetTask(key string) (Task, error) {
	var task Task

	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return task, err
	}

	row := db.QueryRow("SELECT * FROM task WHERE id = ?", id)

	sErr := row.Scan(&task.ID, &task.UserID, &task.Task)

	return task, sErr
}

//GetContacts returns users contacts by user_id
func GetContacts(key string) (Contacts, error) {
	var contacts Contacts

	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return contacts, err
	}

	row := db.QueryRow("SELECT * FROM contacts WHERE user_id = ?", id)

	sErr := row.Scan(&contacts.UserID, &contacts.Mail, &contacts.Phone)

	return contacts, sErr
}

//AddCountry send query to MySQL for insert new country
func AddCountry(body []byte) error {
	var newCountry Country

	if errData := json.Unmarshal(body, &newCountry); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}
	if newCountry.Code == "" {
		err := errors.New("Empty entry")
		return err
	}
	_, err := db.Exec("INSERT INTO country (code, name, area, population, capital)VALUES(?, ?, ?, ?, ?)",
		newCountry.Code, newCountry.Name, newCountry.Area, newCountry.Population, newCountry.Capital)
	if err != nil {
		log.Println(err)
	}
	return err
}

//AddUser send query to MySQL for insert new user
func AddUser(body []byte) error {
	var newUser User

	if errData := json.Unmarshal(body, &newUser); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	_, err := db.Exec("INSERT INTO user (id, name, age, male)VALUES(?, ?, ?, ?)",
		newUser.ID, newUser.Name, newUser.Age, newUser.Male)
	if err != nil {
		log.Println(err)
	}
	return err
}

//AddTask send query to MySQL for insert new users task
func AddTask(body []byte) error {
	var newTask Task

	if errData := json.Unmarshal(body, &newTask); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	_, err := db.Exec("INSERT INTO task (id, user_id, task)VALUES(?, ?, ?)",
		newTask.ID, newTask.UserID, newTask.Task)
	if err != nil {
		log.Println(err)
	}
	return err
}

//AddContacts send query to MySQL for insert new users contacts
func AddContacts(body []byte) error {
	var newContacts Contacts

	if errData := json.Unmarshal(body, &newContacts); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	_, err := db.Exec("INSERT INTO contacts (user_id, mail, phone)VALUES(?, ?, ?)",
		newContacts.UserID, newContacts.Mail, newContacts.Phone)
	if err != nil {
		log.Println(err)
	}
	return err
}

//DelCountry send query to MySQL for delete country by code
func DelCountry(key string) error {
	_, err := db.Exec("DELETE FROM country WHERE code = ?", key)
	return err
}

//DelUser send query to MySQL for delete country by code
func DelUser(key string) error {
	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return err
	}
	_, err = db.Exec("DELETE FROM user WHERE id = ?", id)
	return err
}

//DelTask send query to MySQL for delete country by code
func DelTask(key string) error {
	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return err
	}
	_, err = db.Exec("DELETE FROM task WHERE id = ?", id)
	return err
}

//DelContacts send query to MySQL for delete country by code
func DelContacts(key string) error {
	id, err := strconv.ParseInt(key, 10, 64)
	if err != nil {
		return err
	}
	_, err = db.Exec("DELETE FROM contacts WHERE user_id = ?", id)
	return err
}

//UpdateCountry make changes in data
func UpdateCountry(code string, body []byte) error {
	var updateParam Country

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	var query []string
	var args []interface{}

	if updateParam.Code != "" {
		query = append(query, "code=?")
		args = append(args, updateParam.Code)
	}
	if updateParam.Name != "" {
		query = append(query, "name=?")
		args = append(args, updateParam.Name)
	}
	if updateParam.Area != 0 {
		query = append(query, "area=?")
		args = append(args, updateParam.Area)
	}
	if updateParam.Population != 0 {
		query = append(query, "population=?")
		args = append(args, updateParam.Population)
	}
	if updateParam.Capital != "" {
		query = append(query, "capital=?")
		args = append(args, updateParam.Capital)
	}
	args = append(args, code)

	if len(query) == 0 {
		log.Println("Did not have rows for update")
		err := errors.New("Nothing to update")
		return err
	}

	_, err := db.Exec("UPDATE country SET "+strings.Join(query, ",")+" WHERE code = ?", args...)

	return err
}

//UpdateUser make changes in data
func UpdateUser(id string, body []byte) error {
	var updateParam User

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	var query []string
	var args []interface{}

	if updateParam.ID != 0 {
		query = append(query, "id=?")
		args = append(args, updateParam.ID)
	}
	if updateParam.Name != "" {
		query = append(query, "name=?")
		args = append(args, updateParam.Name)
	}
	if updateParam.Age != 0 {
		query = append(query, "age=?")
		args = append(args, updateParam.Age)
	}
	if updateParam.Male != "" {
		query = append(query, "male=?")
		args = append(args, updateParam.Male)
	}
	args = append(args, id)

	if len(query) == 0 {
		log.Println("Did not have rows for update")
		err := errors.New("Nothing to update")
		return err
	}

	_, err := db.Exec("UPDATE user SET "+strings.Join(query, ",")+" WHERE id = ?", args...)

	return err
}

//UpdateTask make changes in data
func UpdateTask(id string, body []byte) error {
	var updateParam Task

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	var query []string
	var args []interface{}

	if updateParam.ID != 0 {
		query = append(query, "id=?")
		args = append(args, updateParam.ID)
	}
	if updateParam.UserID != 0 {
		query = append(query, "user_id=?")
		args = append(args, updateParam.UserID)
	}
	if updateParam.Task != "" {
		query = append(query, "task=?")
		args = append(args, updateParam.Task)
	}
	args = append(args, id)

	if len(query) == 0 {
		log.Println("Did not have rows for update")
		err := errors.New("Nothing to update")
		return err
	}

	_, err := db.Exec("UPDATE task SET "+strings.Join(query, ",")+" WHERE id = ?", args...)

	return err
}

//UpdateContacts make changes in data
func UpdateContacts(id string, body []byte) error {
	var updateParam Contacts

	if errData := json.Unmarshal(body, &updateParam); errData != nil {
		log.Println(errData)
		err := errors.New("Invalid data")
		return err
	}

	var query []string
	var args []interface{}

	if updateParam.UserID != 0 {
		query = append(query, "user_id=?")
		args = append(args, updateParam.UserID)
	}
	if updateParam.Mail != "" {
		query = append(query, "mail=?")
		args = append(args, updateParam.Mail)
	}
	if updateParam.Phone != "" {
		query = append(query, "phone=?")
		args = append(args, updateParam.Phone)
	}
	args = append(args, id)

	if len(query) == 0 {
		log.Println("Did not have rows for update")
		err := errors.New("Nothing to update")
		return err
	}

	_, err := db.Exec("UPDATE contacts SET "+strings.Join(query, ",")+" WHERE user_id = ?", args...)

	return err
}

//UsersTasks returns users tasks by user_id
func UsersTasks(id string) ([]Task, error) {
	uID, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		return nil , err
	}
	rows, err := db.Query("SELECT * FROM task WHERE user_id =?", uID)
	if err != nil {
		return nil, err
	}

	//log.Println(rows)
	defer rows.Close()

	result := make([]Task, 0)
	for rows.Next() {
		var task Task
		sErr := rows.Scan(&task.ID, &task.UserID, &task.Task)

		if sErr != nil {
			return nil, sErr
		}

		result = append(result, task)

		if sErr = rows.Err(); sErr != nil {
			return nil, sErr
		}

	}
	log.Println(result)

	if len(result) == 0{
		err = errors.New("Not found ")
		return nil, err
	}
	return result, nil
}
