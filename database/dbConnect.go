package database

import (
	"database/sql"
	"crud_mysql/config"
)

var db *sql.DB

//Open database
func Open() error {
	newDb , err := sql.Open("mysql", config.ReadDBConfig())
	if err != nil{
		return err
	}

	err = newDb.Ping()
	if err != nil{
		return err
	}

	db = newDb
	return nil
}

//Close database
func Close() {
	db.Close()
}
