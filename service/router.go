package service

import (
	"net/http"

	"github.com/gorilla/mux"
)

//Router creation
func Router() http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/", Check).Methods("GET")
	router.HandleFunc("/{data}", PrintAll).Methods("GET")
	router.HandleFunc("/{data}/{key}", Print).Methods("GET")
	router.HandleFunc("/insert/{data}", Insert).Methods("POST")
	router.HandleFunc("/update/{data}/{key}", Update).Methods("PUT")
	router.HandleFunc("/remove/{data}/{key}", Delete)

	router.HandleFunc("/user/tasks/{id}", PrintTasks).Methods("GET")

	return router
}
