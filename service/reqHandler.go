package service

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"

	"crud_mysql/database"
	"github.com/gorilla/mux"
)

//PrintAll is responses with all elements from database
func PrintAll(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	data := vars["data"]

	var result []interface{}
	var dErr error

	switch data {
	case "country":
		result, dErr = database.GetCountries()
	case "user":
		result, dErr = database.GetUsers()
	case "task":
		result, dErr = database.GetTasks()
	case "contacts":
		result, dErr = database.GetAllContacts()
	default:
		dErr = errors.New("Not found ")
	}

	w.Header().Set("content-type", "application/json")

	if dErr != nil {
		hErr := ErrorHandler(dErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}
	json.NewEncoder(w).Encode(result)
}

//Print element by key
func Print(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	data := vars["data"]
	key := vars["key"]

	var result interface{}
	var dErr error

	switch data {
	case "country":
		result, dErr = database.GetCountry(key)
	case "user":
		result, dErr = database.GetUser(key)
	case "task":
		result, dErr = database.GetTask(key)
	case "contacts":
		result, dErr = database.GetContacts(key)
	default:
		dErr = errors.New("Not found ")
	}

	w.Header().Set("content-type", "application/json")

	if dErr != nil {
		hErr := ErrorHandler(dErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	json.NewEncoder(w).Encode(result)
}

//Insert JSON object in database
func Insert(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	vars := mux.Vars(r)
	data := vars["data"]

	body, err := ioutil.ReadAll(r.Body)

	w.Header().Set("content-type", "application/json")

	if err != nil {
		log.Println(err)
		inErr := errors.New("Cannot read body ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	var dErr error

	switch data {
	case "country":
		dErr = database.AddCountry(body)
	case "user":
		dErr = database.AddUser(body)
	case "task":
		dErr = database.AddTask(body)
	case "contacts":
		dErr = database.AddContacts(body)
	default:
		dErr = errors.New("Not found ")
	}

	hErr := ErrorHandler(dErr)
	json.NewEncoder(w).Encode(hErr)
}

//Delete removes element by key from database
func Delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	data := vars["data"]
	key := vars["key"]

	var  dErr error

	switch data {
	case "country":
		dErr = database.DelCountry(key)
	case "user":
		dErr = database.DelUser(key)
	case "task":
		dErr = database.DelTask(key)
	case "contacts":
		dErr = database.DelContacts(key)
	default:
		dErr = errors.New("Not found ")
	}

	w.Header().Set("content-type", "application/json")
	hErr := ErrorHandler(dErr)
	json.NewEncoder(w).Encode(hErr)
}

//Update data from database
func Update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	data := vars["data"]
	key := vars["key"]

	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)

	w.Header().Set("content-type", "application/json")

	if err != nil {
		log.Println(err)
		inErr := errors.New("Cannot read body ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	if len(body) == 0 {
		inErr := errors.New("Nothing to update ")
		hErr := ErrorHandler(inErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	var dErr error

	switch data {
	case "country":
		dErr = database.UpdateCountry(key, body)
	case "user":
		dErr = database.UpdateUser(key, body)
	case "task":
		dErr = database.UpdateTask(key, body)
	case "contacts":
		dErr = database.UpdateContacts(key, body)
	default:
		dErr = errors.New("Not found ")
	}

	hErr := ErrorHandler(dErr)
	json.NewEncoder(w).Encode(hErr)
}

//PrintTasks is responses with all user tasks from database
func PrintTasks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	result, dErr := database.UsersTasks(vars["id"])

	w.Header().Set("content-type", "application/json")

	if dErr != nil {
		hErr := ErrorHandler(dErr)
		json.NewEncoder(w).Encode(hErr)
		return
	}

	json.NewEncoder(w).Encode(result)
}
