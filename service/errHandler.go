package service

import (
	"log"
	"strings"
)

//MyErr describes custom error entity
type MyErr struct {
	Status bool
	Error string `json:",omitempty"`
}


//ErrorHandler returns error for server response
func ErrorHandler(err error) MyErr {
	var hErr MyErr
	hErr.Status = true
	if err != nil{
		log.Println(err)
		hErr.Status = false
		switch true {
		case strings.Contains(err.Error(), "Cannot read body "):
			hErr.Error = "Cannot read body"
		case strings.Contains(err.Error(), "Duplicate entry"):
			hErr.Error = "Duplicate entry"
		case strings.Contains(err.Error(), "Empty entry"):
			hErr.Error = "Compulsory field is empty"
		case strings.Contains(err.Error(), "Invalid data"):
			hErr.Error = "Invalid data"
		case strings.Contains(err.Error(), "Nothing to update"):
			hErr.Error = "Nothing to update"
		case strings.Contains(err.Error(), "Not found"):
			hErr.Error = "Data not found"
		case strings.Contains(err.Error(), "key constraint fails"):
			hErr.Error = "Failed in compulsory field"
		default:
			hErr.Error = "Something wrong"
		}
	}
	return hErr
}



