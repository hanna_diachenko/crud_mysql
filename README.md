## golang, CRUD with MySQL

Golang REST API with MySQL for store the countries and users with their tasks and contacts.

#### Dependencies:
* Golang 1.10
* MySQL 5.7

#### Install Go libraries
` go get -u github.com/gorilla/mux `

`go get -u github.com/go-sql-driver/mysql`

#### Configure environment variables:

* LOG_FILE - file path to write server logs
* BD_CONNECTION  - path to the database. e.g `root:pass@tcp(localhost:3306)/`

#### Build
`go build`

#### Run
`./crud`

API is on localhost:8080/

Router config is in the `service/router.go` file.
