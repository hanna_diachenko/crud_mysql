package main

import (
	"log"
	"net/http"
	"os"

	"crud_mysql/config"
	"crud_mysql/database"
	"crud_mysql/service"
	_ "github.com/go-sql-driver/mysql"
)

var f *os.File // logFile handle

func init() {
	logFile := config.ReadLogFileConfig()
	file, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	f = file
	//set output of logs to f
	log.SetOutput(f)

	dbErr := database.InitDB()
	if dbErr != nil {
		log.Fatalf("Failed to init database: %v/n", dbErr)
	}
}

func main() {
	defer database.Close()
	defer end()

	router := service.Router()
	log.Println("API in port 8080")
	log.Println(http.ListenAndServe(":8080", router))
}

func end() {
	if f != nil {
		defer f.Close()
	}
}
